const mysql = require('mysql');
const express = require('express');
var app = express();
const bodyparser = require('body-parser');

app.use(bodyparser.json());
app.use( ( req, res, next ) => {
    res.header( 'Access-Control-Allow-Origin', '*' );
    res.header( 'Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT' );
    res.header( 'Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization' );
    next();
} );
var mysqlConnection = mysql.createConnection({
    host : 'localhost',
    user: 'kms',
    password : 'kms',
    database : 'employeedb',
    multipleStatements: true
});

mysqlConnection.connect((err)=>{
    if(!err)
        console.log('DB Connection Succeded.');
    else
        console.log('DB Connection failed \n Error :' + JSON.stringify(err, undefined, 2));
})

app.listen(5000, ()=>console.log('Express Server is running at port no : 5000'));


//Get all employees
app.get('/employees',(req, res)=>{
    mysqlConnection.query('SELECT * FROM employee', (err, rows, fields)=>{
        if(!err)
            res.send(rows);
        else
            console.log(err);
    })
});

//Get an employees
app.get('/employees/:id',(req, res)=>{
    mysqlConnection.query('SELECT * FROM employee WHERE EmpID = ?', [req.params.id], (err, rows, fields)=>{
        if(!err)
            res.send(rows);
        else
            console.log(err);
    })
});

//Insert an employees
app.post('/employees',(req, res)=>{
    let emp = req.body;
    var sql = "SET @EmpID = ?;SET @Name = ?; SET @EmpCode = ?;SET @Salary = ?; \
    CALL EmployeeAddOrEdit(@EmpID, @Name, @EmpCode, @Salary);";
    mysqlConnection.query(sql,[emp.EmpID, emp.Name, emp.EmpCode, emp.Salary], (err, rows, fields)=>{
        if(!err)
            rows.forEach(element => {
                if(element.constructor == Array)
                res.send('Inserted employee id:' +element[0].EmpID);
            });
        else
            console.log(err);
    })
});

//Update an employees
app.put('/employees',(req, res)=>{
    let emp = req.body;
    var sql = "SET @EmpID = ?;SET @Name = ?; SET @EmpCode = ?;SET @Salary = ?; \
    CALL EmployeeAddOrEdit(@EmpID, @Name, @EmpCode, @Salary);";
    mysqlConnection.query(sql,[emp.EmpID, emp.Name, emp.EmpCode, emp.Salary], (err, rows, fields)=>{
        if(!err)
            res.send('Updated Successfully!');
        else
            console.log(err);
    })
});